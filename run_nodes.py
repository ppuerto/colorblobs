import json
import docker

client = docker.from_env()

with open('sequence.json') as json_file:
	sequence = json.load(json_file)

with open('dict_neig.json') as json_file:
	dict_neig = json.load(json_file)

def deploy_node(name, group, searching, teaching, neighbors):
	cmd = " ".join([name, group, str(searching), str(teaching)] + neighbors)
	client.containers.run(
		"blob", command=cmd, auto_remove=True, detach=True, network="host"
	)

for i, grp in enumerate(sequence):
    group = "GRP{}".format(i)
    for blob in grp:
        deploy_node(blob, group, 0.1, 0.1, dict_neig[blob])