# ColorBlobs

## HowTo

### Install

```bash
$ git clone https://gitlab.com/ppuerto/colorblobs.git
$ cd colorblobs
$ pip install virtualenv
$ virtualenv -p python3 venv
```

#### Activate virtual environment (LINUX)
```bash
$ source venv/bin/activate
(venv)$ pip install -r requirements.txt
```

#### Activate virtual environment (WINDOWS)
```bash
$ venv\Scripts\activate.bat
(venv)$ pip install -r requirements.txt
```

#### Deal with Jupyter Kernel

- Add kernel
```bash
(venv)$ python -m ipykernel install --user --name=venv
```

- Launch Jupyter
```bash
(venv)$ jupyter notebook
```

- Remove kernel
```bash
(venv)$ jupyter kernelspec uninstall venv
```

### Build Docker image

```bash
$ cd app/
$ docker build -t blob .
$ cd ..
```

### Run

1. run `Main.ipynb` and wait for the log `Waiting for nodes ...`
2. run `run_nodes.py`:
```bash 
(venv)$ python run_nodes.py
```
3. enjoy

## Usefull links
- [host.docker.internal](https://stackoverflow.com/questions/31324981/how-to-access-host-port-from-docker-container)