import ast
import zmq
import docker

client = docker.from_env()
context = zmq.Context()

pub = context.socket(zmq.PUB)
pub.bind("tcp://*:5555")

pull = context.socket(zmq.PULL)
pull.bind("tcp://*:5556")


def wait_for_nodes_to_be_ready(total_number_of_nodes):
	print("Waiting for nodes ...")
	nodes = []
	while len(nodes) < total_number_of_nodes:
		new_node = pull.recv_string()
		nodes.append(new_node)
		print("---> {:>2}/{}".format(len(nodes), total_number_of_nodes), end="\r")

	print('All nodes are ready !')
	init_state = {node: "red" for node in nodes}
	return init_state


def run_one_group_step(step, group, nbr_of_nodes_in_group, state):
	pub.send_string("{}|STEP|{}|{}".format(group, step, state))

	n = 0
	while n < nbr_of_nodes_in_group:
		message = pull.recv_string()
		node, _, step, new_state = message.split("|")
		new_state = ast.literal_eval(new_state)

		state[node] = new_state[node]
		n += 1


def run_simulation(nbr_of_steps, sequence_of_groups, init_state):
	state = dict(init_state)

	for step in range(nbr_of_steps):
		print("STEP {:>2}/{}".format(step+1, nbr_of_steps), end='\r')
		for group, nbr_of_nodes in sequence_of_groups.items():
			run_one_group_step(step, group, nbr_of_nodes, state)

	pub.send_string("ALL EXIT")
	print("Simulation done !")
	return state
