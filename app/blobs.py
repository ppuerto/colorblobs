import ast
import sys
import zmq
import time
import numpy as np
import matplotlib.colors as mcolors


COLORS = list(mcolors.CSS4_COLORS.keys())


class Blob:
	def __init__(self, name, neighbors, group="GRP0", searching=0.5, teaching=0.5):
		self.name = name
		self.group = group
		self.searching = searching
		self.teaching = teaching
		self.neighbors = neighbors

		self.list_of_known_colors = ["red"]

	def search(self):
		if np.random.uniform(0, 1) < self.searching:
			self.learn(np.random.choice(COLORS))

	def learn(self, color):
		if color not in self.list_of_known_colors:
			self.list_of_known_colors.append(color)

	def teach(self, state):
		if np.random.uniform(0, 1) < self.teaching:
			state[self.name] = np.random.choice(self.list_of_known_colors)

	def step(self, state):
		time.sleep(np.random.uniform(0, 0.5))
		for blob in self.neighbors:
			self.learn(state[blob])

		self.search()
		self.teach(state)


class WrapBlob:
	def __init__(self, name, neighbors, group, searching, teaching):
		self.blob = Blob(name, neighbors, group, searching, teaching)

		context = zmq.Context()

		self.push = context.socket(zmq.PUSH)
		self.push.connect("tcp://localhost:5556")
		
		self.sub = context.socket(zmq.SUB)
		self.sub.connect("tcp://localhost:5555")
		self.sub.setsockopt_string(zmq.SUBSCRIBE, group)
		self.sub.setsockopt_string(zmq.SUBSCRIBE, "ALL")

		self.push.send_string(name)

	def run(self):
		while True:
			message = self.sub.recv_string()

			if "EXIT" in message:
				break

			message_split = message.split("|")
			state = ast.literal_eval(message_split[-1])
			step = int(message_split[2])

			self.blob.step(state)

			self.push.send_string("{}|DONE|{}|{}".format(self.blob.name, step, state))


if __name__ == "__main__":
	name, group, searching, teaching = sys.argv[1:5]
	neighbors = sys.argv[5:]

	node = WrapBlob(name, neighbors, group, float(searching), float(teaching))
	node.run()
